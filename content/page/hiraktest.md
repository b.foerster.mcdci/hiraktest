---
title: Hirak Archive
subtitle: Proteste in Algerien
comments: false
---

The 2019–2021 Algerian protests, also called Revolution of Smiles[27][28] or Hirak Movement,[29] began on 16 February 2019,[2][30] six days after Abdelaziz Bouteflika announced his candidacy for a fifth presidential term in a signed statement. These protests, without precedent since the Algerian Civil War, were peaceful and led the military to insist on Bouteflika's immediate resignation, which took place on 2 April 2019.[31] By early May, a significant number of power-brokers close to the deposed administration, including the former president's younger brother Saïd, had been arrested.[

### Infos

The rising tensions within the Algerian regime can be traced back to the beginning of Bouteflika's rule which has been characterized by the state's monopoly on natural resources revenues used to finance the government's clientelist system and ensure its stability.[34] The major demonstrations have taken place in the largest urban centers of Algeria from February to December 2019. Due to their significant scale, the protests attracted international media coverage and provoked reactions from several heads of states and scholarly figures. 
