---
title: PDF Viewer
subtitle: Making a Gallery with pdfs
date: 2022-06-20
tags: ["hirak", "Dokumente", "pdf"]
---

## Une manifestation contre un 5e mandat de Bouteflika

<object width="500" height="600" data="/docs/article.pdf" type="application/pdf" width="750px" height="750px">
    <embed src="/docs/article.pdf" type="application/pdf">
        <p>This browser does not support PDFs. </p>
    </embed>
</object>

[Une manifestation contre un 5e mandat de Bouteflika](/docs/article.pdf)

<object width="500" height="600" data="/docs/pdf3.pdf" type="application/pdf" width="750px" height="750px">
    <embed src="/docs/pdf3.pdf" type="application/pdf">
        <p>This browser does not support PDFs. </p>
    </embed>
</object>


[2018.10.30, BBC Arabic, الجزائر لماذا يصر الحزب الحاكم على ترشيح الرئيس بوتفليقة لولاية خامسة؟](/docs/pdf3.pdf)


