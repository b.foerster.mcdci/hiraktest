---
title: PDF Viewer
subtitle: Making a Gallery with pdfs
date: 2022-06-20
tags: ["videos", "hirak"]
---

## Audio & Video

### 2019.02.26, Anes Tina - No you can't

{{< video "/docs/video.mp4" >}}

### 2016.02.21, جيناكم من فلسطين يا الجزائر نور العين (تصميمي)

{{< video "/docs/2016.02.21, جيناكم من فلسطين يا الجزائر نور العين (تصميمي).mp4" >}}
