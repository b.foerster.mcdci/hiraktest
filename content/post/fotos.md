---
title: Fotos
subtitle: Hirakproteste
tags: ["Fotos", "Proteste"]
---
## Proteste in Algerien

{{< gallery caption-effect="fade" >}}
  {{< figure link="/docs/2019.04.20, Stockholm, diasproa protests.jpg" caption="2019.04.20, Stockholm, diasproa protests" >}}
  {{< figure link="/docs/2019.04.21, Mauritanie.jpg" caption="Mauritanie" >}}
  {{< figure link="/docs/2019.04.21, Rabat, مسيرة مليونية من أجل المطالبة بإطلاق سراح معتقلي حراك الريف.jpg" caption="2019.04.21, Rabat, مسيرة مليونية من أجل المطالبة بإطلاق سراح معتقلي حراك الريف" >}}
    {{< figure link="/docs/2019.12.20, Bejaia, Bougie TV, Facebook (2).jpg" caption="2019.04.20, Stockholm, diasproa protests" >}}
  {{< figure link="/docs/pic.jpg" caption="Person" >}}
  {{< figure link="/docs/52883783_2194504500571539_3182609661559832576_o.jpg" caption="event" >}}
{{< /gallery >}}

## Infos
Additional info?
```


