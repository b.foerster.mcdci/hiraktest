---
title: Some Pictures
subtitle: Making a Gallery
date: 2017-03-20
tags: ["example", "photoswipe"]
---
 Ein wenig Text hier

{{< gallery caption-effect="fade" >}}
  {{< figure link="/bird.jpg" caption="Bird" >}}
  {{< figure link="/balloons.jpg" caption="Balloons in the sky" >}}
  {{< figure link="/bonfire.jpg" caption="Bonfire" alt="A bonfire at the beach" >}}
{{< /gallery >}}

<!--more-->
## Infos
Pictures taken from unsplash

```


