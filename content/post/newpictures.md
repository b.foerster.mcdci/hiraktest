---
title: Coding gallery
subtitle: Making a Gallery
date: 2022-06-20
tags: ["example", "photoswipe"]
---
 Ein wenig Text hier

{{< gallery caption-effect="fade" >}}
  {{< figure link="/code.jpg" caption="Random Computer code" >}}
  {{< figure link="/matrix.jpg" caption="Matrix Style" >}}
  {{< figure link="/laptop.jpg" caption="Another picture" >}}
{{< /gallery >}}

## Infos
Pictures taken from unsplash. Random coding pictures. Maybe some buzzwording for search experience added here?

```


